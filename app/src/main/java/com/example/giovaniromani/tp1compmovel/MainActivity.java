package com.example.giovaniromani.tp1compmovel;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    TextToSpeech tts;
    String call;
    int count;



    private TextView tvDisplayTime;
    private TimePicker timePicker1;
    private Button btnChangeTime,bt2;

    private int hour, segundosTotais, segundosRestantes;
    private int minute;
    private Timer timer;



    static final int TIME_DIALOG_ID = 999;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        tvDisplayTime = (TextView) findViewById(R.id.tvTime);
        // timePicker1 = (TimePicker) findViewById(R.id.timePicker1);

        final Calendar c = Calendar.getInstance();
        hour = 0;
        minute = 0;

        // set current time into textview
        tvDisplayTime.setText(new StringBuilder().append(pad(hour)).append(":")
                .append(pad(minute)));
        addListenerOnButton();


    }

    public void addListenerOnButton() {
        bt2 = (Button) findViewById(R.id.bt2);
        bt2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

            }

        });

        btnChangeTime = (Button) findViewById(R.id.btnChangeTime);

        btnChangeTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(TIME_DIALOG_ID);

            }

        });

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:
                // set time picker as current time
                //return new TimePickerDialog(this, timePickerListener, hour, minute,false);
                RelativeLayout linearLayout = new RelativeLayout(this);
                final NumberPicker aNumberPicker = new NumberPicker(this);
                aNumberPicker.setMaxValue(60);
                aNumberPicker.setMinValue(1);

                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
                RelativeLayout.LayoutParams numPicerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                numPicerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
                linearLayout.setLayoutParams(params);
                linearLayout.addView(aNumberPicker,numPicerParams);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle("Selecione o tempo:");
                alertDialogBuilder.setView(linearLayout);
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        tvDisplayTime = (TextView) findViewById(R.id.tvTime);

                                        hour = aNumberPicker.getValue();
                                        minute=0;

                                        segundosTotais = aNumberPicker.getValue()*60;

                                        // set current time into textview
                                        tvDisplayTime.setText(new StringBuilder().append(pad(hour)).append(":")
                                                .append(pad(minute)));

                                        count=0;
                                        final Timer T=new Timer();
                                        T.scheduleAtFixedRate(new TimerTask() {
                                            @Override
                                            public void run() {
                                                runOnUiThread(new Runnable()
                                                {
                                                    @Override
                                                    public void run()
                                                    {
                                                        tvDisplayTime.setText("count="+count);
                                                        count++;
                                                        segundosRestantes = segundosTotais-count;
                                                        if (segundosRestantes==0)
                                                        {
                                                            T.cancel();
                                                            tvDisplayTime.setText(new StringBuilder().append(pad(0)).append(":")
                                                                    .append(pad(0)));

                                                        }
                                                        else
                                                        {
                                                            int minutes = segundosRestantes / 60;
                                                            int seconds = segundosRestantes % 60;
                                                            tvDisplayTime.setText(new StringBuilder().append(pad(minutes)).append(":")
                                                                    .append(pad(seconds)));
                                                        }

                                                    }
                                                });
                                            }
                                        }, 1000, 1000);

                                    }
                                })
                        .setNegativeButton("Cancelar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

        }
        return null;
    }



    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int selectedHour,
                              int selectedMinute) {
            hour = selectedHour;
            minute = selectedMinute;

            // set current time into textview
            tvDisplayTime.setText(new StringBuilder().append(pad(hour))
                    .append(":").append(pad(minute)));

            // set current time into timepicker
           // timePicker1.setCurrentHour(hour);
            //timePicker1.setCurrentMinute(minute);

        }
    };

    private static String pad(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

//ENVIAR PARA A CLASSE
    public void search()
    {
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        int phoneType = pCur.getInt(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.TYPE));
                        String phoneNumber = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                       // TextView tt1 = (TextView) findViewById(R.id.t1);
                        //tt1.setText("Nome " + name + "------ Número: "+phoneNumber);
                        /*switch (phoneType) {
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                Log.d(name + "(mobile number)", phoneNumber);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                Log.d(name + "(home number)", phoneNumber);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                Log.d(name + "(work number)", phoneNumber);
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_OTHER:
                                Log.d(name + "(other number)", phoneNumber);
                                break;
                            default:
                                break;
                        }*/
                    }
                    pCur.close();
                }
            }
        }
    }

    public void call(String mobileNumber)
    {
        String uri = "tel:" + mobileNumber.trim();
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        startActivity(intent);
    }

    public void speak(String texto)
    {
        call = texto;
        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                // TODO Auto-generated method stub
                if (status != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.ENGLISH);
                    tts.speak(call, TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        });
    }

    public void message(String mobileNumber)
    {
        String message = "Hello World!";

        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(mobileNumber, null, message, null, null);
    }

    public void getLocation()
    {
        /*http://stackoverflow.com/questions/17519198/how-to-get-the-current-location-latitude-and-longitude-in-android
        http://developer.android.com/intl/pt-br/training/location/display-address.html
        http://javapapers.com/android/get-current-location-in-android/
http://stackoverflow.com/questions/2227292/how-to-get-latitude-and-longitude-of-the-mobile-device-in-android
http://stackoverflow.com/questions/4256829/getting-street-name-from-address-location-object-in-android
http://stackoverflow.com/questions/9409195/how-to-get-complete-address-from-latitude-and-longitude
http://stackoverflow.com/questions/1513485/how-do-i-get-the-current-gps-location-programmatically-in-android*/


        GPSTracker gps = new GPSTracker(this);
        if (gps.getIsGPSTrackingEnabled())
        {
/*
            String stringLatitude = String.valueOf(gpsTracker.latitude);
            textview = (TextView)findViewById(R.id.fieldLatitude);
            textview.setText(stringLatitude);

            String stringLongitude = String.valueOf(gpsTracker.longitude);
            textview = (TextView)findViewById(R.id.fieldLongitude);
            textview.setText(stringLongitude);

            String country = gpsTracker.getCountryName(this);
            textview = (TextView)findViewById(R.id.fieldCountry);
            textview.setText(country);

            String city = gpsTracker.getLocality(this);
            textview = (TextView)findViewById(R.id.fieldCity);
            textview.setText(city);

            String postalCode = gpsTracker.getPostalCode(this);
            textview = (TextView)findViewById(R.id.fieldPostalCode);
            textview.setText(postalCode);

*/

            String addressLine = gps.getAddressLine(this);
         //   TextView tt1 = (TextView) findViewById(R.id.t1);
           // tt1.setText(addressLine);
        }

/*
        Geocoder gc = new Geocoder(this);
        try{
            if(gc.isPresent()) {
                List<Address> list = gc.getFromLocation(37.42279, -122.08506, 1);

                Address address = list.get(0);

                StringBuffer str = new StringBuffer();
                str.append("Name: " + address.getLocality() + "\n");
                str.append("Sub-Admin Ares: " + address.getSubAdminArea() + "\n");
                str.append("Admin Area: " + address.getAdminArea() + "\n");
                str.append("Country: " + address.getCountryName() + "\n");
                str.append("Country Code: " + address.getCountryCode() + "\n");

                String strAddress = str.toString();
                TextView tt1 = (TextView) findViewById(R.id.t1);
                tt1.setText(strAddress);
            }
        }
            catch(Exception e)
            {

            }
*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
